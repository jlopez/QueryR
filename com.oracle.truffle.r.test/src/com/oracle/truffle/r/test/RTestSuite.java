package com.oracle.truffle.r.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import com.oracle.truffle.r.launcher.RMain;

/**
 * This class runs FastR on every R file it can find in given directories (it will search
 * recursively).
 */
public final class RTestSuite {
    private static final String defaultTestsDirectory = "tests";

    public static final void main(String[] args) {
        final AtomicInteger successfulTests = new AtomicInteger(0);
        final AtomicInteger totalTests = new AtomicInteger(0);

        Arrays.asList(args.length == 0 ? new String[]{defaultTestsDirectory} : args).stream().map(
                        dir -> {
                            try {
                                return Files.find(Paths.get(dir), 50,
                                                (p, bfa) -> (p.toString().endsWith(".R") || p.toString().endsWith(".r")) && bfa.isRegularFile() && !p.toString().startsWith("tests/tpch/"));
                            } catch (IOException e) {
                                e.printStackTrace();
                                return null;
                            }
                        }).flatMap(x -> x).forEach(file -> {
                            System.out.println("Testing: " + file);
                            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            final String expected;
                            try {
                                expected = String.join("\n", Files.readAllLines(Paths.get(file.toString().replaceFirst("\\.[Rr]$", ".out"))));
                                RMain.runRscript(new String[]{file.toString()}, System.in, new PrintStream(baos), System.err);
                                if (expected.equals(baos.toString()))
                                    successfulTests.incrementAndGet();
                                else {
                                    System.out.println("[FAILED] Expected: ");
                                    System.out.println(expected);
                                    System.out.println("got: ");
                                    System.out.println(baos.toString());
                                }
                            } catch (IOException e) {
                                System.err.println("[FAILED] Could not find expected output.");
                            }
                            System.out.flush();
                            System.err.flush();
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            totalTests.incrementAndGet();
                        });
        System.out.println("Passed " + successfulTests + "/" + totalTests + " tests.");
    }
}
