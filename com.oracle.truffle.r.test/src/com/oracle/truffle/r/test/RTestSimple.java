package com.oracle.truffle.r.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.oracle.truffle.r.launcher.RMain;

/**
 * This class runs FastR on every R file it can find in given directories (it will search
 * recursively).
 */
public final class RTestSimple {
    public static final void main(String[] args) {
        System.out.println("Testing: " + args[0]);
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final String expected;
        try {
            expected = String.join("\n", Files.readAllLines(Paths.get(args[0].replaceFirst("\\.[Rr]$", ".out"))));
            int resCode = RMain.runRscript(new String[]{args[0]}, System.in, new PrintStream(baos), System.err);
            if (resCode != 0)
                System.out.println("[FAILED] Command exited with code " + resCode);
            else if (expected.equals(baos.toString()))
                System.out.println("[SUCCESS]");
            else {
                System.out.println("[FAILED] Expected: ");
                System.out.println(expected);
                System.out.println("got: ");
                System.out.println(baos.toString());
            }
        } catch (IOException e) {
            System.err.println("[FAILED] Could not find expected output.");
        }
        System.out.flush();
        System.err.flush();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
