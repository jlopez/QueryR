dol_to_euro = function (dol) dol * 89.0 / 100.0
dol_to_euro2 = dol_to_euro

emp = new.tableRef("emp", "PostgreSQL", "postgre.config", "public")
minsalary = 2500.0
q = query.select(function (x) {
             res = new.env()
             res$empno = x$empno
             res$ename = x$ename
             res$salary = dol_to_euro2(x$sal)
             res },
    query.where(function (x) x$sal >= minsalary,
    query.from(emp)))
results = query.force(q)
print(results)
