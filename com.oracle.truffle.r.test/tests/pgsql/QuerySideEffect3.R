emp = new.tableRef("emp", "PostgreSQL", "postgre.config", "public")
minsalary = 2500.0
q = query.select(function (x) {
             res = new.env()
             res$empno = x$empno
             res$ename = x$ename
	     print(paste("salary:", x$sal))
             res$salary = (function (dol){
               a = dol * 89.0 / 100.0
               while (a > 1000.0) a = a * 89.0 / 100.0
               a
             })(x$sal)
             res },
    query.where(function (x) x$sal >= minsalary,
    query.from(emp)))
results = query.force(q)
print(results)
