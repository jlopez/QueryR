part = new.tableRef("part", "PostgreSQL", "postgre.config", "tpch")
lineitem = new.tableRef("lineitem", "PostgreSQL", "postgre.config", "tpch")
q =
query.select(function(x){
  y = new.env()
  y$revenue = sum(x$l_extendedprice * (1 - x$l_discount))
  y
},
query.where(function(x)
  (x$p_brand == 'Brand#12' && match(x$p_container, c('SM CASE', 'SM BOX', 'SM PACK', 'SM PKG'))
  && x$l_quantity >= 1 && x$l_quantity <= 1 + 10 && x$p_size >= 1 && x$p_size <= 5
  && match(x$l_shipmode, c('AIR', 'AIR REG')) && x$l_shipinstruct == 'DELIVER IN PERSON')
  || (x$p_brand == 'Brand#23' && match(x$p_container, c('MED BAG', 'MED BOX', 'MED PKG', 'MED PACK'))
  && x$l_quantity >= 10 && x$l_quantity <= 10 + 10 && x$p_size >= 1 && x$p_size <= 10
  && match(x$l_shipmode, c('AIR', 'AIR REG')) && x$l_shipinstruct == 'DELIVER IN PERSON')
  || (x$p_brand == 'Brand#34' && match(x$p_container, c('LG CASE', 'LG BOX', 'LG PACK', 'LG PKG'))
  && x$l_quantity >= 20 && x$l_quantity <= 20 + 10 && x$p_size >= 1 && x$p_size <= 15
  && match(x$l_shipmode, c('AIR', 'AIR REG')) && x$l_shipinstruct == 'DELIVER IN PERSON')
,
query.join(query.from(part),
     query.from(lineitem),
     function(x, y) x$p_partkey == y$l_partkey)))
print(query.force(q))
