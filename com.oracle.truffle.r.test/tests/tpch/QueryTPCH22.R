lineitem = new.tableRef("lineitem", "PostgreSQL", "postgre.config", "tpch")
customer = new.tableRef("customer", "PostgreSQL", "postgre.config", "tpch")
orders = new.tableRef("orders", "PostgreSQL", "postgre.config", "tpch")
q =
query.order(function(x) c(x$cntrycode), function(x) c(TRUE),
query.group(function(x) c(x$cntrycode),
query.select(function(x){
  y = new.env()
  y$cntrycode = x$cntrycode
  y$numcust = length()
  y$totacctbal = sum(x$c_acctbal)
  y
},
query.from(
  query.select(function(x){
    y = new.env()
    y$cntrycode = substr(x$c_phone, 1, 2)
    y$c_acctbal = x$c_acctbal
    y
  },
  query.where(function(x) match(substr(x$c_phone, 1, 2), c('13', '31', '23', '29', '30', '18', '17'))
  && x$c_acctbal > query.force(
    query.select(function(y){
      z = new.env()
      z$avg_bal = mean(y$c_acctbal)
      z
    },
    query.where(function(y) y$c_acctbal > 0.00
    && match(substr(y$c_phone, 1, 2), c('13', '31', '23', '29', '30', '18', '17'))
    ,
    query.from(customer))))
  && query.force(
    query.select(function(y){
      z = new.env()
      z$count_orders = length()
      z
    },
    query.where(function(y) y$o_custkey > x$c_custkey,
    query.from(orders)))) == 0,
  query.from(customer)))
))))
print(query.force(q))
