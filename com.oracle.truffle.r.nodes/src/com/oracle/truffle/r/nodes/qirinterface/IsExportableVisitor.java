package com.oracle.truffle.r.nodes.qirinterface;

import com.oracle.truffle.api.RootCallTarget;
import com.oracle.truffle.r.nodes.RSyntaxNodeVisitor;
import com.oracle.truffle.r.nodes.access.ConstantNode;
import com.oracle.truffle.r.nodes.access.WriteLocalFrameVariableNode;
import com.oracle.truffle.r.nodes.access.variables.LookupNode;
import com.oracle.truffle.r.nodes.builtin.InternalNode;
import com.oracle.truffle.r.nodes.control.BlockNode;
import com.oracle.truffle.r.nodes.control.BreakNode;
import com.oracle.truffle.r.nodes.control.ForNode;
import com.oracle.truffle.r.nodes.control.IfNode;
import com.oracle.truffle.r.nodes.control.RepeatNode;
import com.oracle.truffle.r.nodes.control.ReplacementDispatchNode;
import com.oracle.truffle.r.nodes.control.WhileNode;
import com.oracle.truffle.r.nodes.function.FunctionDefinitionNode;
import com.oracle.truffle.r.nodes.function.FunctionExpressionNode;
import com.oracle.truffle.r.nodes.function.RCallNode;
import com.oracle.truffle.r.nodes.function.RCallSpecialNode;
import com.oracle.truffle.r.nodes.function.signature.MissingNode;
import com.oracle.truffle.r.nodes.function.signature.QuoteNode;
import com.oracle.truffle.r.nodes.query.RFromNode;
import com.oracle.truffle.r.nodes.query.RGroupNode;
import com.oracle.truffle.r.nodes.query.RJoinNode;
import com.oracle.truffle.r.nodes.query.RLeftJoinNode;
import com.oracle.truffle.r.nodes.query.RLimitNode;
import com.oracle.truffle.r.nodes.query.ROrderNode;
import com.oracle.truffle.r.nodes.query.RQIRWrapperNode;
import com.oracle.truffle.r.nodes.query.RRightJoinNode;
import com.oracle.truffle.r.nodes.query.RSelectNode;
import com.oracle.truffle.r.nodes.query.RWhereNode;
import com.oracle.truffle.r.runtime.Arguments;
import com.oracle.truffle.r.runtime.nodes.RNode;
import com.oracle.truffle.r.runtime.nodes.RSyntaxElement;
import com.oracle.truffle.r.runtime.nodes.RSyntaxNode;

public class IsExportableVisitor implements RSyntaxNodeVisitor<Boolean> {
    public static final IsExportableVisitor instance = new IsExportableVisitor();

    private IsExportableVisitor() {
    }

    @Override
    public final Boolean visit(final IfNode ifNode) {
        return ifNode.getCondition().asRSyntaxNode().accept(this) && ifNode.getThenPart().asRSyntaxNode().accept(this) && ifNode.getElsePart().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final WhileNode whileNode) {
        return whileNode.getCondition().accept(this) && whileNode.getBody().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final ForNode forNode) {
        return forNode.getRange().asRSyntaxNode().accept(this) && forNode.body.asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final BreakNode breakNode) {
        return true;
    }

    /**
     * Translation of a sequence of statements. Note: This is also where the STRAD-ASSIGN rules are
     * handled.
     */
    @Override
    public final Boolean visit(final BlockNode block) {
        final RNode[] children = block.getSequence();

        for (int i = 0; i < children.length; i++)
            if (!children[i].asRSyntaxNode().accept(this))
                return false;
        return true;
    }

    @Override
    public final Boolean visit(final LookupNode var) {
        return true;
    }

    @Override
    public final Boolean visit(final WriteLocalFrameVariableNode var) {
        return var.getRhs().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final FunctionExpressionNode fun) {
        final RootCallTarget target = fun.getCallTarget();
        if (target == null)
            return false;
        return ((FunctionDefinitionNode) target.getRootNode()).getBody().accept(this);
    }

    @Override
    public final Boolean visit(final InternalNode fun) {
        throw new RuntimeException("Error in translation to QIR: InternalNode unsupported.");
    }

    @Override
    public final Boolean visit(final RCallNode call) {
        final Arguments<RSyntaxNode> args = call.getArguments();
        try {
            return visitBuiltin(((LookupNode) call.getFunction()).getIdentifier());
        } catch (RuntimeException e) {
        }
        if (!call.getFunction().asRSyntaxNode().accept(this))
            return false;
        for (int i = 0; i < args.getLength(); i++)
            if (!args.getArgument(i).accept(this))
                return false;
        return true;
    }

    @Override
    public final Boolean visit(final RCallSpecialNode call) {
        final RSyntaxElement[] args = call.getSyntaxArguments();
        if (!visitBuiltin(call.expectedFunction.getName()))
            return false;
        for (int i = 0; i < args.length; i++)
            if (!((RSyntaxNode) args[i]).accept(this))
                return false;
        return true;
    }

    private final static Boolean visitBuiltin(final String name) {
        switch (name) {
            case "c":
            case "$":
            case "+":
            case "-":
            case "*":
            case "/":
            case "==":
            case "!=":
            case "&":
            case "&&":
            case "|":
            case "||":
            case "<=":
            case "<":
            case ">=":
            case ">":
            case "!":
            case "(":
                return true;
            case "print":
                return false;
            default:
                throw new RuntimeException("Unknown call special node: " + name);
        }
    }

    @Override
    public final Boolean visit(final ReplacementDispatchNode repl) {
        return repl.lhs.asRSyntaxNode().accept(this) && repl.rhs.asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RQIRWrapperNode query) {
        return true;
    }

    @Override
    public final Boolean visit(final RSelectNode select) {
        return select.formatter.asRSyntaxNode().accept(this) && select.query.asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RFromNode from) {
        return from.getTable().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RWhereNode where) {
        return where.getFilter().asRSyntaxNode().accept(this) && where.getQuery().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RGroupNode group) {
        return group.getGroup().asRSyntaxNode().accept(this) && group.getQuery().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final ROrderNode order) {
        return order.getIsAscending().asRSyntaxNode().accept(this) && order.getOrder().asRSyntaxNode().accept(this) && order.getQuery().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RJoinNode join) {
        return join.getFilter().asRSyntaxNode().accept(this) && join.getLeft().asRSyntaxNode().accept(this) && join.getRight().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RLeftJoinNode join) {
        return join.getFilter().asRSyntaxNode().accept(this) && join.getLeft().asRSyntaxNode().accept(this) && join.getRight().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RRightJoinNode join) {
        return join.getFilter().asRSyntaxNode().accept(this) && join.getLeft().asRSyntaxNode().accept(this) && join.getRight().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final RLimitNode limit) {
        return limit.getLimit().asRSyntaxNode().accept(this) && limit.getQuery().asRSyntaxNode().accept(this);
    }

    @Override
    public final Boolean visit(final ConstantNode cst) {
        return true;
    }

    @Override
    public final Boolean visit(final MissingNode node) {
        throw new RuntimeException("Cannot translate MissingNode to QIR.");
    }

    @Override
    public final Boolean visit(final QuoteNode node) {
        throw new RuntimeException("Cannot translate QuoteNode to QIR.");
    }

    @Override
    public final Boolean visit(final RepeatNode node) {
        throw new RuntimeException("Cannot translate RepeatNode to QIR.");
    }
}
