package com.oracle.truffle.r.nodes.qirinterface;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.r.nodes.RSyntaxNodeVisitor;
import com.oracle.truffle.r.nodes.access.AccessArgumentNode;
import com.oracle.truffle.r.nodes.access.ConstantNode;
import com.oracle.truffle.r.nodes.access.WriteLocalFrameVariableNode;
import com.oracle.truffle.r.nodes.access.variables.LookupNode;
import com.oracle.truffle.r.nodes.builtin.InternalNode;
import com.oracle.truffle.r.nodes.control.*;
import com.oracle.truffle.r.nodes.function.FunctionExpressionNode;
import com.oracle.truffle.r.nodes.function.RCallNode;
import com.oracle.truffle.r.nodes.function.RCallSpecialNode;
import com.oracle.truffle.r.nodes.function.signature.MissingNode;
import com.oracle.truffle.r.nodes.function.signature.QuoteNode;
import com.oracle.truffle.r.nodes.query.*;
import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.nodes.RNode;
import com.oracle.truffle.r.runtime.nodes.RSyntaxNode;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.QIRBoolean;
import qir.ast.expression.QIRNull;
import qir.ast.expression.QIRNumber;
import qir.ast.expression.arithmetic.*;
import qir.ast.expression.logic.*;
import qir.ast.expression.relational.*;
import qir.ast.operator.*;

/**
 * This visitor translates a {@link RNode} into a {@link QIRNode}.
 */
public final class QIRTranslateVisitor implements RSyntaxNodeVisitor<QIRNode> {
    public static final QIRTranslateVisitor instance = new QIRTranslateVisitor();

    private QIRTranslateVisitor() {
    }

    private final SourceSection dummy = null;

    @Override
    public final QIRNode visit(final IfNode ifNode) {
        return new QIRIf(ifNode.getSourceSection(), ifNode.getCondition().asRSyntaxNode().accept(this), ifNode.getThenPart().asRSyntaxNode().accept(this),
                        ifNode.getElsePart().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final WhileNode whileNode) {
        return whileNode.accept(IsExportableVisitor.instance)
                        ? new QIRExportableTruffleNode(whileNode.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, whileNode.getSourceSection().getCharacters().toString())
                        : new QIRUnexportableTruffleNode(whileNode.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, whileNode.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final ForNode forNode) {
        return forNode.accept(IsExportableVisitor.instance)
                        ? new QIRExportableTruffleNode(forNode.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, forNode.getSourceSection().getCharacters().toString())
                        : new QIRUnexportableTruffleNode(forNode.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, forNode.getSourceSection().getCharacters().toString());
    }

    @Override
    public final QIRNode visit(final BreakNode breakNode) {
        return breakNode.accept(IsExportableVisitor.instance)
                        ? new QIRExportableTruffleNode(breakNode.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, breakNode.getSourceSection().getCharacters().toString())
                        : new QIRUnexportableTruffleNode(breakNode.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, breakNode.getSourceSection().getCharacters().toString());
    }

    /**
     * Translation of a sequence of statements. Note: This is also where the STRAD-ASSIGN rules are
     * handled.
     */
    @Override
    public final QIRNode visit(final BlockNode block) {
        QIRNode result = null;
        final RNode[] children = block.getSequence();
        final int len = children.length;

        if (len == 0) // Block is empty
            return QIRNull.getInstance();

        result = children[len - 1].asRSyntaxNode().accept(this); // The last statement of the block
        /*
         * In this loop, we wrap the result with the other statements of the block in reverse order
         * in the way described by the STRAD-ASSIGN rules.
         */
        for (int i = len - 2; i > -1; i--) {
            final RNode child = children[i];
            if (child instanceof WriteLocalFrameVariableNode) { // STRAD-ASSIGN-ID
                final RNode value = ((WriteLocalFrameVariableNode) child).getRhs();

                // If the assignment value reads an argument, then we translate to a lambda.
                result = new QIRLambda(dummy, null, new QIRVariable(dummy, ((WriteLocalFrameVariableNode) child).getName(), null), result, new FrameDescriptor());
                // Else we apply STRAD-ASSIGN-ID normally
                if (!(value instanceof AccessArgumentNode))
                    result = new QIRApply(dummy, result, value.asRSyntaxNode().accept(this));
            } else if (child instanceof ReplacementDispatchNode && ((ReplacementDispatchNode) child).lhs instanceof LookupNode) {
                final RNode value = ((ReplacementDispatchNode) child).rhs;

                // If the assignment value reads an argument, then we translate to a lambda.
                result = new QIRLambda(dummy, null, new QIRVariable(dummy, ((LookupNode) ((ReplacementDispatchNode) child).lhs).getIdentifier(), null), result, new FrameDescriptor());
                // Else we apply STRAD-ASSIGN-ID normally
                if (!(value instanceof AccessArgumentNode))
                    result = new QIRApply(dummy, result, value.asRSyntaxNode().accept(this));
            } else if (child instanceof ReplacementDispatchNode) { // STRAD-ASSIGN-FIELD and
                                                                   // STRAD-ASSIGN-DOT
                // TODO: Take care of STRAD-ASSIGN-DOT
                // TODO: RCallSpecialNode is not necessarily a write
                final ReplacementDispatchNode repl = (ReplacementDispatchNode) child;
                final RCallSpecialNode lhs = (RCallSpecialNode) repl.lhs;
                final LookupNode receiver = (LookupNode) lhs.getSyntaxArguments()[0];
                final LookupNode eid = (LookupNode) lhs.getSyntaxArguments()[1];
                final RNode value = repl.rhs;
                result = new QIRApply(dummy, new QIRLambda(dummy, null, new QIRVariable(dummy, receiver.getIdentifier(), null), result, new FrameDescriptor()),
                                new QIRRcons(dummy, eid.getIdentifier(), value.asRSyntaxNode().accept(this), receiver.accept(this)));
            } else
                throw new UnsupportedOperationException("Cannot translate \"" + block.getSourceSection() + "\" to QIR: untranslatable sequence.");
        }
        return result;
    }

    @Override
    public final QIRNode visit(final LookupNode var) {
        return new QIRVariable(var.getSourceSection(), var.getIdentifier(), null);
    }

    @Override
    public final QIRNode visit(final WriteLocalFrameVariableNode var) {
        // STRAD-ASSIGN-ID should be handled in BlockNode.
        throw new RuntimeException("Error in translation to QIR: should not have visited an assignment.");
    }

    @Override
    public final QIRNode visit(final FunctionExpressionNode fun) {
        return QIRInterface.RToQIRType(fun.getSourceSection(), fun);
    }

    @Override
    public final QIRNode visit(final InternalNode fun) {
        throw new RuntimeException("Error in translation to QIR: InternalNode unsupported.");
    }

    @Override
    public final QIRNode visit(final RCallNode call) {
        try {
            return visitBuiltin(call.getSourceSection(), ((LookupNode) call.getFunction()).getIdentifier(),
                            Arrays.stream(call.getArguments().getArguments()).map(arg -> arg.accept(this)).collect(Collectors.toList()));
        } catch (final RuntimeException e) {
        }
        final QIRNode fun = call.getFunction().asRSyntaxNode().accept(this);
        final RSyntaxNode[] args = call.getArguments().getArguments();
        final int nbArgs = args.length;
        QIRNode res = fun;

        if (nbArgs == 0) // call is an application with no arguments
            return new QIRApply(dummy, fun, null);
        for (final RSyntaxNode arg : args)
            res = new QIRApply(dummy, res, arg.accept(this));
        return res;
    }

    @Override
    public final QIRNode visit(final RCallSpecialNode call) {
        return visitBuiltin(call.getSourceSection(), call.expectedFunction.getName(),
                        Arrays.stream(call.getSyntaxArguments()).map(arg -> ((RSyntaxNode) arg).accept(this)).collect(Collectors.toList()));
    }

    private static final QIRNode visitBuiltin(final SourceSection src, final String name, final List<QIRNode> args) {
        switch (name) {
            case "c":
                QIRList res = QIRLnil.getInstance();
                for (int i = args.size() - 1; i >= 0; i--)
                    res = new QIRLcons(src, args.get(i), res);
                return res;
            case "$":
                return new QIRRdestr(src, args.get(0), ((QIRVariable) args.get(1)).id);
            case "+":
                return QIRPlusNodeGen.create(src, args.get(0), args.get(1));
            case "-":
                return QIRMinusNodeGen.create(src, args.get(0), args.get(1));
            case "*":
                return QIRStarNodeGen.create(src, args.get(0), args.get(1));
            case "/":
                return QIRDivNodeGen.create(src, args.get(0), args.get(1));
            case "==":
                return QIREqualNodeGen.create(src, args.get(0), args.get(1));
            case "!=":
                return QIRNotNodeGen.create(src, QIREqualNodeGen.create(src, args.get(0), args.get(1)));
            case "&":
            case "&&":
                return QIRAndNodeGen.create(src, args.get(0), args.get(1));
            case "|":
            case "||":
                return QIROrNodeGen.create(src, args.get(0), args.get(1));
            case "<=":
                return QIRLowerOrEqualNodeGen.create(src, args.get(0), args.get(1));
            case "<":
                return QIRLowerThanNodeGen.create(src, args.get(0), args.get(1));
            case ">=":
                return QIRNotNodeGen.create(src, QIRLowerThanNodeGen.create(src, args.get(0), args.get(1)));
            case ">":
                return QIRNotNodeGen.create(src, QIRLowerOrEqualNodeGen.create(src, args.get(0), args.get(1)));
            case "!":
                return QIRNotNodeGen.create(src, args.get(0));
            case "(":
                if (args.size() == 1)
                    return args.get(0);
            default:
                throw new RuntimeException("Unknown call special node: " + name);
        }
    }

    @Override
    public final QIRNode visit(final ReplacementDispatchNode repl) {
        // STRAD-ASSIGN-FIELD and STRAD-ASSIGN-DOT should be handled in BlockNode.
        throw new RuntimeException("Error in translation to QIR: should not have visited an assignment.");
    }

    /**
     * Translation of a subquery.
     */
    @Override
    public final QIRNode visit(final RQIRWrapperNode query) {
        return RContext.queries.get(query.id);
    }

    @Override
    public final QIRNode visit(final RSelectNode select) {
        return new QIRProject(select.getSourceSection(), select.formatter.asRSyntaxNode().accept(this), select.query.asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RFromNode from) {
        return new QIRScan(from.getSourceSection(), from.getTable().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RWhereNode where) {
        return new QIRFilter(where.getSourceSection(), where.getFilter().asRSyntaxNode().accept(this), where.getQuery().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RGroupNode group) {
        return new QIRGroupBy(group.getSourceSection(), group.getGroup().asRSyntaxNode().accept(this), group.getQuery().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final ROrderNode order) {
        final QIRNode config1 = order.getOrder().asRSyntaxNode().accept(this);
        final QIRLambda tmp = (QIRLambda) order.getIsAscending().asRSyntaxNode().accept(this);
        final QIRList config2body = ((QIRList) tmp.getBody()).map(e -> QIRBoolean.create(e.equals(new QIRNumber(null, 1))));
        return new QIRSortBy(order.getSourceSection(), config1, new QIRLambda(tmp.getSourceSection(), tmp.getFunName(), tmp.getVar(), config2body, new FrameDescriptor()),
                        order.getQuery().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RJoinNode join) {
        return new QIRJoin(join.getSourceSection(), join.getFilter().asRSyntaxNode().accept(this), join.getLeft().asRSyntaxNode().accept(this), join.getRight().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RLeftJoinNode join) {
        return new QIRLeftJoin(join.getSourceSection(), join.getFilter().asRSyntaxNode().accept(this), join.getLeft().asRSyntaxNode().accept(this), join.getRight().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RRightJoinNode join) {
        return new QIRRightJoin(join.getSourceSection(), join.getFilter().asRSyntaxNode().accept(this), join.getLeft().asRSyntaxNode().accept(this), join.getRight().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final RLimitNode limit) {
        return new QIRLimit(limit.getSourceSection(), limit.getLimit().asRSyntaxNode().accept(this), limit.getQuery().asRSyntaxNode().accept(this));
    }

    @Override
    public final QIRNode visit(final ConstantNode cst) {
        return QIRInterface.RToQIRType(cst.getSourceSection(), cst.getValue());
    }

    @Override
    public final QIRNode visit(final MissingNode node) {
        throw new RuntimeException("Cannot translate MissingNode to QIR.");
    }

    @Override
    public final QIRNode visit(final QuoteNode node) {
        throw new RuntimeException("Cannot translate QuoteNode to QIR.");
    }

    @Override
    public final QIRNode visit(final RepeatNode node) {
        throw new RuntimeException("Cannot translate RepeatNode to QIR.");
    }
}