/*
 * Copyright (c) 2015, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.qirinterface;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

import com.oracle.truffle.api.frame.Frame;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameSlot;
import com.oracle.truffle.api.object.DynamicObject;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.r.nodes.builtin.RBuiltinRootNode;
import com.oracle.truffle.r.nodes.function.FunctionDefinitionNode;
import com.oracle.truffle.r.nodes.function.FunctionExpressionNode;
import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.data.RFunction;
import com.oracle.truffle.r.runtime.data.RPromise;
import com.oracle.truffle.r.runtime.data.RStringVector;
import com.oracle.truffle.r.runtime.data.RVector;
import com.oracle.truffle.r.runtime.data.model.RAbstractContainer;
import com.oracle.truffle.r.runtime.env.REnvironment;

import qir.ast.*;
import qir.ast.data.*;
import qir.ast.expression.*;
import qir.driver.DBDriver;
import qir.driver.QIRDriver;

public final class QIRInterface {
    /**
     * Runs a query in the database pointed by the given driver and returns the results.
     *
     * @param query The query to be executed in the targeted database
     * @return The results of the query
     */
    public static final Map<String, List<Object>> run(final QIRNode query) {
        // Run the query and retrieve results
        QIRNode qirRes = QIRDriver.run(query);

        // An error during the evaluation of the query
        if (qirRes == null)
            throw new RuntimeException("Could not run query: " + query);

        // The query did not return the right type
        if (!(qirRes instanceof QIRList))
            throw new RuntimeException("Internal error: query returned a non-list type");

        // The query returned an empty result
        if (qirRes == QIRLnil.getInstance())
            return new HashMap<>();

        final Map<String, List<Object>> results = new HashMap<>();

        // Inhabit the result columns with empty lists (we need the keys to create the lists) TODO:
        // Find a way to add the first row here and skip it on the next loop
        for (QIRNode firstRow = ((QIRLcons) qirRes).getValue(); firstRow != QIRRnil.getInstance(); firstRow = ((QIRRcons) firstRow).getTail())
            results.put(((QIRRcons) firstRow).getId(), new ArrayList<>());

        // Process the rest of the rows TODO: Error if a row has unknown column
        for (; qirRes != QIRLnil.getInstance(); qirRes = ((QIRLcons) qirRes).getTail())
            for (QIRNode row = ((QIRLcons) qirRes).getValue(); row != QIRRnil.getInstance(); row = ((QIRRcons) row).getTail())
                results.get(((QIRRcons) row).getId()).add(QIRToRType(((QIRRcons) row).getValue()));

        return results;
    }

    /**
     * Resolves the free variables of a query.
     *
     * @param arg The query to be executed in the targeted database
     * @param argFrame The environment of execution
     * @return The closed query
     */
    public static final QIRNode normalize(final QIRNode arg, final Frame argFrame) {
        QIRNode query = arg;
        final SourceSection dummy = null;

        for (Map<String, QIRVariable> fvs = QIRDriver.getFreeVars(query); !fvs.isEmpty(); fvs = QIRDriver.getFreeVars(query))
            for (final QIRVariable fv : fvs.values()) {
                final String varName = fv.id;
                Frame frame = argFrame;
                FrameSlot varSlot = frame.getFrameDescriptor().findFrameSlot(varName);
                for (; varSlot == null && frame.getArguments()[4] instanceof Frame && ((Frame) frame.getArguments()[4]).getArguments()[4] instanceof Frame; frame = (Frame) frame.getArguments()[4])
                    varSlot = ((Frame) frame.getArguments()[4]).getFrameDescriptor().findFrameSlot(varName);
                query = new QIRApply(dummy, new QIRLambda(dummy, null, new QIRVariable(dummy, varName, varSlot), query, new FrameDescriptor()),
                                varName.equals("interval") ? new QIRExternal(dummy, "interval")
                                                : varName.equals("having") ? new QIRExternal(dummy, "having") // TODO:
                                                                                                              // Remove
                                                                                                              // this
                                                                                                              // hack
                                                                : RToQIRType(fv.getSourceSection(), varSlot != null ? frame.getValue(varSlot) : RContext.getInstance().lookupBuiltin(varName)));
            }
        return query;
    }

    /**
     * This function closes the connection of database drivers.
     */
    public static final void closeDrivers() {
        DBDriver.closeDrivers();
    }

    /**
     * Translates a QIR expression into a R statement.
     *
     * @param value The QIR expression to translate
     * @return The translation of the QIR expression in R
     * @throws UnsupportedOperationException If the type of the value is not supported.
     */
    @SuppressWarnings("unchecked")
    static final <T> T QIRToRType(final QIRNode value) throws UnsupportedOperationException {
        if (value instanceof QIRBaseValue<?>)
            return ((QIRBaseValue<T>) value).getValue();
        throw new RuntimeException("Unsupported value: " + value);
    }

    /**
     * Translates a R statement into a QIR expression.
     *
     * @param src The {@link SourceSection} of the given value
     * @param value The R statement to translate
     * @return The translation of the R statement in QIR
     */
    static final <T> QIRNode RToQIRType(final SourceSection src, final T value) {
        if (value instanceof BigInteger)
            return new QIRBigNumber(src, (BigInteger) value);
        if (value instanceof Integer)
            return new QIRNumber(src, (Integer) value);
        if (value instanceof Byte)
            return new QIRNumber(src, (Byte) value);
        if (value instanceof Long)
            return new QIRNumber(src, (Long) value);
        if (value instanceof Double) {
            return new QIRDouble(src, (Double) value);
        }
        if (value instanceof Boolean)
            return QIRBoolean.create((Boolean) value);
        if (value instanceof String)
            return new QIRString(src, (String) value);
        if (value instanceof REnvironment) {
            REnvironment env = (REnvironment) value;
            final Object queryId = env.get("queryId");
            if (queryId != null) // The object is a query
                return normalize(RContext.queries.get((Integer) queryId), RContext.envs.get((Integer) queryId));
            final String tableName = (String) env.get("tableName");
            final String schemaName = (String) env.get("schemaName");
            final String dbName = (String) env.get("dbName");
            final String configFile = (String) env.get("configFile");
            if (tableName != null && dbName != null && configFile != null) // The object is a table
                return new QIRTable(src, new QIRString(null, tableName), new QIRString(null, dbName), new QIRString(null, configFile), new QIRString(null, schemaName));
            // else, the object is considered a record
            QIRRecord tuple = QIRRnil.getInstance();
            for (final Object x : env.getFrame().getFrameDescriptor().getIdentifiers())
                tuple = new QIRRcons(src, (String) x, RToQIRType(src, env.get((String) x)), tuple);
            return tuple;
        }
        if (value instanceof FunctionExpressionNode) {
            return RFunctionToQIRType(src, null, (FunctionDefinitionNode) (((FunctionExpressionNode) value).getCallTarget().getRootNode()));
        }
        if (value instanceof RFunction) {
            final RFunction fun = (RFunction) value;
            switch (fun.getName()) {
                case "new.env":
                    return new QIRLambda(src, "new.env", new QIRVariable(null, "_", null), QIRRnil.getInstance(), new FrameDescriptor());
                case "return":
                case "(":
                case "query.force": {
                    final QIRVariable x = new QIRVariable(null, "x", null);
                    return new QIRLambda(src, "identity", x, x, new FrameDescriptor());
                }
                case "new.tableRef":
                    final QIRVariable tableName = new QIRVariable(null, "__tmp__");
                    final QIRVariable dbName = new QIRVariable(null, "__tmp2__");
                    final QIRVariable configFile = new QIRVariable(null, "__tmp3__");
                    final QIRVariable schemaName = new QIRVariable(null, "__tmp4__");
                    return new QIRLambda(null, null, tableName,
                                    new QIRLambda(null, null, dbName, new QIRLambda(null, null, configFile,
                                                    new QIRLambda(null, null, schemaName, new QIRTable(src, tableName, dbName, configFile, schemaName),
                                                                    new FrameDescriptor()),
                                                    new FrameDescriptor()),
                                                    new FrameDescriptor()),
                                    new FrameDescriptor());
                case "sum":
                case "min":
                case "max":
                case "date":
                    return new QIRExternal(src, fun.getName());
                case "substr":
                    return new QIRExternal(src, "substring");
                case "mean":
                    return new QIRExternal(src, "avg");
                case "length":
                    return new QIRExternal(src, "count");
                case "match":
                    return new QIRExternal(src, "match");
                case "format": {
                    final QIRVariable x = new QIRVariable(null, "x", null);
                    final QIRVariable y = new QIRVariable(null, "y", null);
                    return new QIRLambda(null, null, x, new QIRLambda(null, null, y, new QIRApply(null, new QIRExternal(src, "extractYear"), x), new FrameDescriptor()),
                                    new FrameDescriptor());
                }
                case "regexpr":
                    final QIRVariable x = new QIRVariable(null, "x", null);
                    final QIRVariable y = new QIRVariable(null, "y", null);
                    return new QIRLambda(null, null, x, new QIRLambda(null, null, y, new QIRApply(null, new QIRApply(null, new QIRExternal(src, "like"), y), x), new FrameDescriptor()),
                                    new FrameDescriptor());
                default:
                    return fun.getRootNode() instanceof RBuiltinRootNode ? new QIRExportableTruffleNode(src, "r", QIRInterface::execute, QIRInterface::apply, fun.getName())
                                    : RFunctionToQIRType(src, fun.getName(), (FunctionDefinitionNode) fun.getRootNode());
            }
        }
        if (value instanceof RPromise) {
            final RPromise fun = (RPromise) value;
            if (fun.isEvaluated())
                return RToQIRType(src, fun.getValue());
            return RToQIRType(src, fun.getClosure().eval(fun.getFrame()));
        }
        if (value instanceof RVector) {
            final QIRNode[] nodes = RVectorToQIR((RVector<?>) value);
            QIRList res = QIRLnil.getInstance();

            for (int i = nodes.length - 1; i >= 0; i--)
                res = new QIRLcons(null, nodes[i], res);
            return res;
        }
        throw new RuntimeException("Unsupported value: " + value);
    }

    private static final QIRNode[] RVectorToQIR(final RVector<?> vector) {
        final DynamicObject attrs = vector.getAttributes();
        QIRNode[] res = null;

        if (attrs == null) { // regular vector
            res = new QIRNode[vector.getLength()];
            for (int i = 0; i < res.length; i++)
                res[i] = RToQIRType(null, vector.getDataAtAsObject(i));
        } else if (!attrs.containsKey("class")) { // unknown
            throw new RuntimeException("Unsupported value: " + vector);
        } else {
            final Object type = ((RStringVector) attrs.get("class")).getDataAt(0);
            if (type.equals("data.frame")) {
                final QIRNode[][] data = new QIRNode[vector.getLength()][];
                for (int i = 0; i < data.length; i++)
                    data[i] = RVectorToQIR((RVector<?>) vector.getDataAtAsObject(i));
                res = new QIRNode[data[0].length];
                QIRRecord t;
                RStringVector v;
                for (int i = 0; i < res.length; i++) {
                    t = QIRRnil.getInstance();
                    v = ((RStringVector) attrs.get("names"));
                    for (int j = 0; j < v.getLength(); j++)
                        t = new QIRRcons(null, v.getDataAt(j), data[j][i], t);
                    res[i] = t;
                }
            } else if (type.equals("factor")) {
                final RVector<?> realData = (RVector<?>) attrs.get("levels");
                res = new QIRNode[realData.getLength()];
                for (int i = 0; i < res.length; i++)
                    res[i] = RToQIRType(null, realData.getDataAtAsObject((Integer) vector.getDataAtAsObject(i) - 1));
            } else
                throw new RuntimeException("Unsupported value: " + vector);
        }
        return res;
    }

    public static final Object getRValueFromTruffle(final Value v) {
        Object o = v.getReceiver();

        // FastR puts single values in a container?
        if (o instanceof RAbstractContainer) {
            final RAbstractContainer rac = (RAbstractContainer) o;
            if (rac.getLength() != 1)
                throw new IllegalArgumentException("Unhandled value: " + rac.toString());
            o = rac.getDataAtAsObject(0);
        }
        if (o instanceof Serializable || o instanceof RFunction || o instanceof REnvironment)
            return o;
        return v.getSourceLocation().getCharacters().toString();
    }

    static final QIRNode execute(final String program) {
        Context rContext;
        try {
            rContext = Context.getCurrent();
        } catch (IllegalStateException e) {
            try {
                rContext = Context.newBuilder("R").build();
            } catch (IllegalArgumentException e2) {
                throw new RuntimeException(e2.getMessage());
            }
        }

        try {
            final org.graalvm.polyglot.Source source = org.graalvm.polyglot.Source.newBuilder("R", program, "mySrc").build();
            final Value result = rContext.eval(source);
            return RToQIRType(null, getRValueFromTruffle(result));
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            throw new RuntimeException(sw.toString());
        }
    }

    static final QIRTruffleNode apply(final QIRTruffleNode fun, final List<QIRNode> args) {
        int argIndex = 1;
        String code = "";
        List<String> applyArgs = new ArrayList<>();

        for (QIRNode arg : args) {
            if (arg instanceof QIRRecord) {
                final String argName = "res" + argIndex;
                code += "\n" + argName + " = new.env();";
                for (QIRNode v = arg; v instanceof QIRRcons; v = ((QIRRcons) v).getTail()) {
                    final Object value = QIRToRType(((QIRRcons) v).getValue());
                    code += "\n" + argName + "$" + ((QIRRcons) v).getId() + " = " + (value instanceof String ? "\"" + value + "\"" : value) + ";";
                }
                applyArgs.add(argName);
            } else
                applyArgs.add(QIRToRType(arg).toString());
        }
        code += "\n(" + fun.getCode() + ")(" + applyArgs.stream().collect(Collectors.joining(",")) + ")";
        return fun instanceof QIRExportableTruffleNode ? new QIRExportableTruffleNode(fun.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, code)
                        : new QIRUnexportableTruffleNode(fun.getSourceSection(), "r", QIRInterface::execute, QIRInterface::apply, code);
    }

    private static final QIRNode RFunctionToQIRType(final SourceSection src, final String funName, final FunctionDefinitionNode fun) {
        try {
            QIRNode res = fun.getBody().accept(QIRTranslateVisitor.instance);
            final String[] args = fun.getSignature().getNames();

            if (args.length == 0)
                return new QIRLambda(src, funName, null, res, new FrameDescriptor());
            for (int i = args.length - 1; i >= 0; i--)
                res = new QIRLambda(src, funName, new QIRVariable(null, args[i], null), res, new FrameDescriptor());
            return res;
        } catch (UnsupportedOperationException e) {
            final SourceSection funSrc = fun.getCallTarget().getRootNode().getSourceSection();
            // TODO: Handle dependencies
            return fun.getBody().accept(IsExportableVisitor.instance) ? new QIRExportableTruffleNode(funSrc, "r", QIRInterface::execute, QIRInterface::apply, funSrc.getCharacters().toString())
                            : new QIRUnexportableTruffleNode(funSrc, "r", QIRInterface::execute, QIRInterface::apply, funSrc.getCharacters().toString());
        }
    }
}