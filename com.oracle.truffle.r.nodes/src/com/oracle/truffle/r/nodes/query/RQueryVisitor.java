package com.oracle.truffle.r.nodes.query;

import java.util.Arrays;

import com.oracle.truffle.r.nodes.RSyntaxNodeVisitor;
import com.oracle.truffle.r.nodes.access.ConstantNode;
import com.oracle.truffle.r.nodes.access.WriteLocalFrameVariableNode;
import com.oracle.truffle.r.nodes.access.variables.LookupNode;
import com.oracle.truffle.r.nodes.builtin.InternalNode;
import com.oracle.truffle.r.nodes.control.*;
import com.oracle.truffle.r.nodes.function.FunctionExpressionNode;
import com.oracle.truffle.r.nodes.function.RCallNode;
import com.oracle.truffle.r.nodes.function.RCallSpecialNode;
import com.oracle.truffle.r.nodes.function.signature.MissingNode;
import com.oracle.truffle.r.nodes.function.signature.QuoteNode;
import com.oracle.truffle.r.nodes.qirinterface.QIRTranslateVisitor;
import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.nodes.RNode;
import com.oracle.truffle.r.runtime.nodes.RSyntaxNode;

import qir.ast.QIRNode;

/**
 * This visitor detects queries in a R statement and calls {@link QIRTranslateVisitor} to translate
 * them into {@link QIRNode}.
 */
public final class RQueryVisitor implements RSyntaxNodeVisitor<RSyntaxNode> {
    public static final RQueryVisitor instance = new RQueryVisitor();

    private RQueryVisitor() {
    }

    @Override
    public final RSyntaxNode visit(final IfNode ifNode) {
        return new IfNode(ifNode.getSourceSection(), ifNode.getSyntaxLHS(), ifNode.getCondition().asRSyntaxNode().accept(this), ifNode.getThenPart().asRSyntaxNode().accept(this),
                        ifNode.getElsePart() == null ? null : ifNode.getElsePart().asRSyntaxNode().accept(this));
    }

    @Override
    public final RSyntaxNode visit(final WhileNode whileNode) {
        return new WhileNode(whileNode.getSourceSection(), whileNode.getSyntaxLHS(), whileNode.getCondition().accept(this), whileNode.getBody().asRSyntaxNode().accept(this));
    }

    @Override
    public final RSyntaxNode visit(final ForNode forNode) {
        return forNode;
    }

    @Override
    public final RSyntaxNode visit(final BreakNode breakNode) {
        return breakNode;
    }

    @Override
    public final RSyntaxNode visit(final BlockNode block) {
        final RNode[] children = block.getSequence();

        for (int i = 0; i < children.length; i++)
            children[i] = children[i].asRSyntaxNode().accept(this).asRNode();
        return block;
    }

    @Override
    public final RSyntaxNode visit(final LookupNode var) {
        return var;
    }

    @Override
    public final RSyntaxNode visit(final WriteLocalFrameVariableNode var) {
        return WriteLocalFrameVariableNode.create(var.getName(), var.mode, var.getRhs().asRSyntaxNode().accept(this).asRNode()).asRSyntaxNode();
    }

    @Override
    public final RSyntaxNode visit(final FunctionExpressionNode fun) {
        return fun;
    }

    @Override
    public final RSyntaxNode visit(final InternalNode fun) {
        return fun;
    }

    @Override
    public final RSyntaxNode visit(final RCallNode call) {
        return RCallNode.createCall(call.getSourceSection(), call.getFunction().asRSyntaxNode().accept(this).asRNode(), call.getSyntaxSignature(),
                        Arrays.asList(call.getArguments().getArguments()).stream().map(arg -> arg != null ? arg.accept(this) : arg).toArray(RSyntaxNode[]::new));
    }

    @Override
    public final RSyntaxNode visit(final RCallSpecialNode call) {
        return RCallSpecialNode.createCall(call.getSourceSection(), call.getFunctionNode().asRSyntaxNode().accept(this).asRNode(), call.getSyntaxSignature(),
                        Arrays.asList((RSyntaxNode[]) call.getSyntaxArguments()).stream().map(arg -> arg.accept(this)).toArray(RSyntaxNode[]::new));
    }

    @Override
    public final RSyntaxNode visit(final ReplacementDispatchNode repl) {
        return new ReplacementDispatchNode(repl.getSourceSection(), repl.getSyntaxLHS(), repl.lhs.asRSyntaxNode().accept(this), repl.rhs.asRSyntaxNode().accept(this), repl.isSuper,
                        repl.tempNamesStartIndex);
    }

    @Override
    public final RSyntaxNode visit(final RQIRWrapperNode qir) {
        return qir;
    }

    @Override
    public final RSyntaxNode visit(final RSelectNode select) {
        final RQIRWrapperNode res = new RQIRWrapperNode(select.getSourceSection());
        RContext.queries.set(res.id, select.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RFromNode from) {
        final RQIRWrapperNode res = new RQIRWrapperNode(from.getSourceSection());
        RContext.queries.set(res.id, from.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RWhereNode where) {
        final RQIRWrapperNode res = new RQIRWrapperNode(where.getSourceSection());
        RContext.queries.set(res.id, where.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RGroupNode group) {
        final RQIRWrapperNode res = new RQIRWrapperNode(group.getSourceSection());
        RContext.queries.set(res.id, group.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final ROrderNode order) {
        final RQIRWrapperNode res = new RQIRWrapperNode(order.getSourceSection());
        RContext.queries.set(res.id, order.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RJoinNode join) {
        final RQIRWrapperNode res = new RQIRWrapperNode(join.getSourceSection());
        RContext.queries.set(res.id, join.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RLeftJoinNode join) {
        final RQIRWrapperNode res = new RQIRWrapperNode(join.getSourceSection());
        RContext.queries.set(res.id, join.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RRightJoinNode join) {
        final RQIRWrapperNode res = new RQIRWrapperNode(join.getSourceSection());
        RContext.queries.set(res.id, join.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final RLimitNode limit) {
        final RQIRWrapperNode res = new RQIRWrapperNode(limit.getSourceSection());
        RContext.queries.set(res.id, limit.accept(QIRTranslateVisitor.instance));
        return res;
    }

    @Override
    public final RSyntaxNode visit(final ConstantNode cst) {
        return cst;
    }

    @Override
    public final RSyntaxNode visit(final MissingNode node) {
        return node;
    }

    @Override
    public final RSyntaxNode visit(final QuoteNode node) {
        return node;
    }

    @Override
    public final RSyntaxNode visit(final RepeatNode node) {
        return new RepeatNode(node.getSourceSection(), node.getSyntaxLHS(), node.body);
    }
}
