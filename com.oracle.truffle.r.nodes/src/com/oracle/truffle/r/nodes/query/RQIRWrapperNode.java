/*
 * Copyright (c) 2012, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.query;

import com.oracle.truffle.api.CompilerDirectives.*;
import com.oracle.truffle.api.frame.*;
import com.oracle.truffle.api.nodes.*;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.data.RDataFactory;
import com.oracle.truffle.r.runtime.env.REnvironment;
import com.oracle.truffle.r.runtime.env.REnvironment.PutException;
import com.oracle.truffle.r.runtime.nodes.RSourceSectionNode;
import com.oracle.truffle.r.runtime.nodes.RSyntaxConstant;

@NodeInfo(shortName = "query", description = "The node representing a query")
public final class RQIRWrapperNode extends RSourceSectionNode implements RSyntaxConstant {
    // The unique identifier of the query
    public final int id;

    public RQIRWrapperNode(final SourceSection src) {
        super(src);
        this.id = RContext.createFreshQuery();
    }

    @Override
    public final REnvironment execute(VirtualFrame frame) {
        final int instanceId = RContext.envs.get(id) == null ? id : RContext.createFreshQueryFrom(id);
        RContext.envs.set(instanceId, frame);
        return createQuery(instanceId);
    }

    @TruffleBoundary
    private static final REnvironment createQuery(int instanceId) {
        final REnvironment res = RDataFactory.createNewEnv(null);
        try {
            res.put("queryId", instanceId);
        } catch (PutException e) {
            e.printStackTrace();
            return null;
        }
        return res;
    }

    @Override
    public Object getValue() {
        return createQuery(id);
    }
}
