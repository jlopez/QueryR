package com.oracle.truffle.r.nodes.access.variables;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.source.SourceSection;
import com.oracle.truffle.r.nodes.function.visibility.SetVisibilityNode;
import com.oracle.truffle.r.runtime.nodes.RSourceSectionNode;
import com.oracle.truffle.r.runtime.nodes.RSyntaxLookup;
import com.oracle.truffle.r.runtime.nodes.RSyntaxNode;

public final class LookupNode extends RSourceSectionNode implements RSyntaxNode, RSyntaxLookup {

    @Child private ReadVariableNode read;
    @Child private SetVisibilityNode visibility;

    LookupNode(SourceSection sourceSection, ReadVariableNode read) {
        super(sourceSection);
        this.read = read;
    }

    @Override
    public void voidExecute(VirtualFrame frame) {
        read.executeInternal(frame, frame);
    }

    @Override
    public Object execute(VirtualFrame frame) {
        return read.executeInternal(frame, frame);
    }

    @Override
    public Object visibleExecute(VirtualFrame frame) {
        if (visibility == null) {
            CompilerDirectives.transferToInterpreterAndInvalidate();
            visibility = insert(SetVisibilityNode.create());
        }
        try {
            return read.executeInternal(frame, frame);
        } finally {
            visibility.execute(frame, true);
        }
    }

    @Override
    public String getIdentifier() {
        return read.getIdentifier();
    }

    @Override
    public boolean isFunctionLookup() {
        return read.isFunctionLookup();
    }
}