package com.oracle.truffle.r.nodes;

import com.oracle.truffle.r.nodes.access.ConstantNode;
import com.oracle.truffle.r.nodes.access.WriteLocalFrameVariableNode;
import com.oracle.truffle.r.nodes.access.variables.LookupNode;
import com.oracle.truffle.r.nodes.builtin.InternalNode;
import com.oracle.truffle.r.nodes.control.*;
import com.oracle.truffle.r.nodes.control.IfNode;
import com.oracle.truffle.r.nodes.function.FunctionExpressionNode;
import com.oracle.truffle.r.nodes.function.RCallNode;
import com.oracle.truffle.r.nodes.function.RCallSpecialNode;
import com.oracle.truffle.r.nodes.function.signature.MissingNode;
import com.oracle.truffle.r.nodes.function.signature.QuoteNode;
import com.oracle.truffle.r.nodes.query.*;
import com.oracle.truffle.r.runtime.nodes.IRSyntaxNodeVisitor;
import com.oracle.truffle.r.runtime.nodes.RSyntaxNode;

/**
 * An interface for visitors for R Truffle nodes.
 */
public interface RSyntaxNodeVisitor<T> extends IRSyntaxNodeVisitor<T> {
    public abstract T visit(final IfNode ifNode);

    public abstract T visit(final WhileNode whileNode);

    public abstract T visit(final ForNode forNode);

    public abstract T visit(final BreakNode b);

    public abstract T visit(final BlockNode block);

    public abstract T visit(final LookupNode var);

    public abstract T visit(final WriteLocalFrameVariableNode var);

    public abstract T visit(final FunctionExpressionNode fun);

    public abstract T visit(final InternalNode b);

    public abstract T visit(final RCallNode callNode);

    public abstract T visit(final RCallSpecialNode callNode);

    public abstract T visit(final ReplacementDispatchNode repl);

    public abstract T visit(final RQIRWrapperNode qir);

    public abstract T visit(final RSelectNode select);

    public abstract T visit(final RFromNode from);

    public abstract T visit(final RWhereNode where);

    public abstract T visit(final RGroupNode group);

    public abstract T visit(final ROrderNode order);

    public abstract T visit(final RJoinNode join);

    public abstract T visit(final RLeftJoinNode join);

    public abstract T visit(final RRightJoinNode join);

    public abstract T visit(final RLimitNode limit);

    public abstract T visit(final ConstantNode cst);

    public abstract T visit(final MissingNode node);

    public abstract T visit(final QuoteNode node);

    public abstract T visit(final RepeatNode node);

    @Override
    public default T visit(final RSyntaxNode node) {
        if (node instanceof IfNode)
            return visit((IfNode) node);
        if (node instanceof WhileNode)
            return visit((WhileNode) node);
        if (node instanceof ForNode)
            return visit((ForNode) node);
        if (node instanceof BreakNode)
            return visit((BreakNode) node);
        if (node instanceof BlockNode)
            return visit((BlockNode) node);
        if (node instanceof LookupNode)
            return visit((LookupNode) node);
        if (node instanceof WriteLocalFrameVariableNode)
            return visit((WriteLocalFrameVariableNode) node);
        if (node instanceof FunctionExpressionNode)
            return visit((FunctionExpressionNode) node);
        if (node instanceof InternalNode)
            return visit((InternalNode) node);
        if (node instanceof RCallNode)
            return visit((RCallNode) node);
        if (node instanceof RCallSpecialNode)
            return visit((RCallSpecialNode) node);
        if (node instanceof ReplacementDispatchNode)
            return visit((ReplacementDispatchNode) node);
        if (node instanceof RQIRWrapperNode)
            return visit((RQIRWrapperNode) node);
        if (node instanceof RSelectNode)
            return visit((RSelectNode) node);
        if (node instanceof RFromNode)
            return visit((RFromNode) node);
        if (node instanceof RWhereNode)
            return visit((RWhereNode) node);
        if (node instanceof RGroupNode)
            return visit((RGroupNode) node);
        if (node instanceof ROrderNode)
            return visit((ROrderNode) node);
        if (node instanceof RJoinNode)
            return visit((RJoinNode) node);
        if (node instanceof RLeftJoinNode)
            return visit((RLeftJoinNode) node);
        if (node instanceof RRightJoinNode)
            return visit((RRightJoinNode) node);
        if (node instanceof RLimitNode)
            return visit((RLimitNode) node);
        if (node instanceof ConstantNode)
            return visit((ConstantNode) node);
        if (node instanceof MissingNode)
            return visit((MissingNode) node);
        if (node instanceof QuoteNode)
            return visit((QuoteNode) node);
        if (node instanceof RepeatNode)
            return visit((RepeatNode) node);
        throw new RuntimeException("Internal error: unknown node to visit: " + node.getClass() + ".");
    }
}
