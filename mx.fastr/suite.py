suite = {
  "mxversion" : "5.141.1",
  "name" : "fastr",
  "versionConflictResolution" : "latest",
  "imports" : {
    "suites" : [
            {
               "name" : "truffle",
               "subdir" : True,
               "version" : "a2b64d51297dd7d138f17515db0126d7c68caa17",
               "urls" : [
                    {"url" : "https://github.com/graalvm/graal", "kind" : "git"},
                    {"url" : "https://curio.ssw.jku.at/nexus/content/repositories/snapshots", "kind" : "binary"},
                ]
            },

        ],
   },

  "repositories" : {
    "snapshots" : {
        "url" : "https://curio.ssw.jku.at/nexus/content/repositories/snapshots",
        "licenses" : ["GPLv3"]
    }
  },

  "licenses" : {
    "GPLv3" : {
      "name" : "GNU General Public License, version 3",
      "url" : "https://www.gnu.org/licenses/gpl-3.0.html"
    },
  },

  "defaultLicense" : "GPLv3",

  # libraries that we depend on
  # N.B. The first four with a "path" attribute must be located
  # relative to the suite root and not the mx cache because they are
  # explicitly referenced in the Parser annotation processor.
  # We reference ANTLR twice, because the "path" reference is not
  # properly handled by MX in all cases and causes integration problems.
  "libraries" : {
    "GNUR" : {
        "path" : "libdownloads/R-3.4.0.tar.gz", # keep in sync with the GraalVM support distribution
        "urls" : ["http://cran.rstudio.com/src/base/R-3/R-3.4.0.tar.gz"],
        "sha1" : "054c1d099006354c89b195df6783b933846ced60",
        "resource" : "true"
    },

    "ANTLR-3.5_4_ANNOT_PROC" : {
      "path" : "libdownloads/antlr-runtime-3.5.jar",
      "urls" : ["http://central.maven.org/maven2/org/antlr/antlr-runtime/3.5/antlr-runtime-3.5.jar"],
      "sha1" : "0baa82bff19059401e90e1b90020beb9c96305d7",
    },

    "QIR" : {
      "path" : "../qir/qir.jar",
      "sha1" : "NOCHECK",
    },

    "HADOOP_AUTH" : {
      "path" : "$HBASE_HOME/lib/hadoop-auth-2.5.1.jar",
      "sha1" : "NOCHECK",
    },

    "HADOOP_COMMON" : {
      "path" : "$HBASE_HOME/lib/hadoop-common-2.5.1.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_CLIENT" : {
      "path" : "$HBASE_HOME/lib/hbase-client-1.2.8.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_COMMON" : {
      "path" : "$HBASE_HOME/lib/hbase-common-1.2.8.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_CODEC" : {
      "path" : "$HBASE_HOME/lib/commons-codec-1.9.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_LOG" : {
      "path" : "$HBASE_HOME/lib/commons-logging-1.2.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_COLLECTIONS" : {
      "path" : "$HBASE_HOME/lib/commons-collections-3.2.2.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_CONFIGURATION" : {
      "path" : "$HBASE_HOME/lib/commons-configuration-1.6.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_ZOOKEEPER" : {
      "path" : "$HBASE_HOME/lib/zookeeper-3.4.6.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_SLF4J" : {
      "path" : "$HBASE_HOME/lib/slf4j-api-1.7.7.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_SLF4J_LOG4J" : {
      "path" : "$HBASE_HOME/lib/slf4j-log4j12-1.7.5.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_LOG4J" : {
      "path" : "$HBASE_HOME/lib/log4j-1.2.17.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_PROTOCOL" : {
      "path" : "$HBASE_HOME/lib/hbase-protocol-1.2.8.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_NETTY" : {
      "path" : "$HBASE_HOME/lib/netty-all-4.0.50.Final.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_METRICS" : {
      "path" : "$HBASE_HOME/lib/metrics-core-2.2.0.jar",
      "sha1" : "NOCHECK",
    },

    "HBASE_HTRACE_CORE_INCUBATING" : {
      "path" : "$HBASE_HOME/lib/htrace-core-3.1.0-incubating.jar",
      "sha1" : "NOCHECK",
    },

    "HIVE_EXEC" : {
      "path" : "$HIVE_HOME/lib/hive-exec-2.1.1.jar",
      "sha1" : "NOCHECK",
    },

    "HIVE_JDBC" : {
      "path" : "$HIVE_HOME/lib/hive-jdbc-2.1.1.jar",
      "sha1" : "NOCHECK",
    },

    "HIVE_SERVICE" : {
      "path" : "$HIVE_HOME/lib/hive-service-2.1.1.jar",
      "sha1" : "NOCHECK",
    },

    "HIVE_HTTPCLIENT" : {
      "path" : "$HIVE_HOME/lib/httpclient-4.4.jar",
      "sha1" : "NOCHECK",
    },

    "HIVE_HTTPCORE" : {
      "path" : "$HIVE_HOME/lib/httpcore-4.4.jar",
      "sha1" : "NOCHECK",
    },

    "PGSQL" : {
      "path" : "../lib/postgresql.jar",
      "sha1" : "NOCHECK",
    },

    "OJDBC" : {
      "path" : "../lib/ojdbc.jar",
      "sha1" : "NOCHECK",
    },

    "ANTLR-C-3.5" : {
      "path" : "libdownloads/antlr-complete-3.5.1.jar",
      "urls" : ["http://central.maven.org/maven2/org/antlr/antlr-complete/3.5.1/antlr-complete-3.5.1.jar"],
      "sha1" : "ebb4b995fd67a9b291ea5b19379509160f56e154",
    },

    "ANTLR-3.5" : {
      "sha1" : "0baa82bff19059401e90e1b90020beb9c96305d7",
      "maven" : {
        "groupId" : "org.antlr",
        "artifactId" : "antlr-runtime",
        "version" : "3.5",
      },
    },

    "XZ-1.6" : {
      "sha1" : "05b6f921f1810bdf90e25471968f741f87168b64",
      "maven" : {
        "groupId" : "org.tukaani",
        "artifactId" : "xz",
        "version" : "1.6",
      },
    },
  },

  "projects" : {
    "com.oracle.truffle.r.parser.processor" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "ANTLR-3.5_4_ANNOT_PROC",
        "ANTLR-C-3.5",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "Truffle,FastR",
    },

    "com.oracle.truffle.r.parser" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.parser.processor",
        "com.oracle.truffle.r.runtime",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : ["TRUFFLE_R_PARSER_PROCESSOR"],
      "workingSets" : "Truffle,FastR",
    },

    "com.oracle.truffle.r.nodes" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.runtime",
        "QIR"
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : [
          "truffle:TRUFFLE_DSL_PROCESSOR",
      ],
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.nodes.builtin" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.library",
        "QIR"
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : [
        "truffle:TRUFFLE_DSL_PROCESSOR",
      ],
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.nodes.test" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.test",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "Truffle,FastR,Test",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.test" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "mx:JUNIT",
        "truffle:TRUFFLE_TCK",
        "com.oracle.truffle.r.engine",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "Truffle,FastR,Test",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.test.native" : {
      "native" : True,
      "sourceDirs" : [],
      "dependencies" : ["com.oracle.truffle.r.native"],
      "platformDependent" : True,
      "output" : "com.oracle.truffle.r.test.native",
      "results" :[
         "urand/lib/liburand.so",
         "urand/lib/liburand.sol",
       ],
      "workingSets" : "FastR",
    },

    "com.oracle.truffle.r.test.packages" : {
      "sourceDirs" : ["r"],
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR",
    },

    "com.oracle.truffle.r.test.packages.analyzer" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "mx:JUNIT"
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR",
    },

    "com.oracle.truffle.r.engine" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.nodes.builtin",
        "com.oracle.truffle.r.parser",
        "truffle:JLINE",
        "truffle:TRUFFLE_NFI",
        "QIR",
	"HADOOP_AUTH",
	"HADOOP_COMMON",
	"HBASE_CLIENT",
	"HBASE_COMMON",
	"HBASE_CODEC",
	"HBASE_LOG",
	"HBASE_COLLECTIONS",
	"HBASE_CONFIGURATION",
	"HBASE_SLF4J",
	"HBASE_SLF4J_LOG4J",
	"HBASE_LOG4J",
	"HBASE_ZOOKEEPER",
	"HBASE_PROTOCOL",
	"HBASE_METRICS",
	"HBASE_HTRACE_CORE_INCUBATING",
	"HBASE_NETTY",
        "HIVE_EXEC",
	"HIVE_JDBC",
	"HIVE_SERVICE",
	"HIVE_HTTPCLIENT",
	"HIVE_HTTPCORE",
        "PGSQL",
        "OJDBC",
      ],
     "generatedDependencies" : [
        "com.oracle.truffle.r.parser",
     ],

      "annotationProcessors" : [
          "truffle:TRUFFLE_DSL_PROCESSOR",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.runtime" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.launcher",
        "truffle:TRUFFLE_API",
        "XZ-1.6",
        "QIR"
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : [
          "truffle:TRUFFLE_DSL_PROCESSOR",
      ],
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.launcher" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "sdk:GRAAL_SDK",
        "sdk:LAUNCHER_COMMON",
        "truffle:JLINE",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : [
      ],
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.legacylauncher" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "sdk:GRAAL_SDK",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : [
      ],
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.ffi.impl" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
         "com.oracle.truffle.r.ffi.processor",
         "com.oracle.truffle.r.nodes"
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "annotationProcessors" : [
          "truffle:TRUFFLE_DSL_PROCESSOR",
          "R_FFI_PROCESSOR",
      ],
      "workingSets" : "Truffle,FastR",
      "jacoco" : "include",
    },

    "com.oracle.truffle.r.ffi.codegen" : {
      "sourceDirs" : ["src"],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "dependencies" : [
        "com.oracle.truffle.r.ffi.impl"
      ],
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR",
    },

    "com.oracle.truffle.r.ffi.processor" : {
      "sourceDirs" : ["src"],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR",
    },

    "com.oracle.truffle.r.native" : {
      "sourceDirs" : [],
#      "class" : "FastRNativeProject",
      "dependencies" : [
        "GNUR",
        "truffle:TRUFFLE_NFI_NATIVE",
      ],
      "native" : True,
      "single_job" : True,
      "workingSets" : "FastR",
      "buildEnv" : {
        "NFI_INCLUDES" : "-I<path:truffle:TRUFFLE_NFI_NATIVE>/include",
      },
    },

    "com.oracle.truffle.r.library" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.ffi.impl",
      ],
      "annotationProcessors" : [
          "truffle:TRUFFLE_DSL_PROCESSOR",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR",
      "jacoco" : "include",

    },

    "com.oracle.truffle.r.library.fastrGrid.server" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "com.oracle.truffle.r.library",
      ],
      "annotationProcessors" : [
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR",
      "jacoco" : "include",

    },

    "com.oracle.truffle.r.release" : {
      "sourceDirs" : ["src"],
      "buildDependencies" : ["com.oracle.truffle.r.native.recommended"],
      "class" : "FastRReleaseProject",
      "output" : "com.oracle.truffle.r.release",
    },

    "com.oracle.truffle.r.native.recommended" : {
      "dependencies" : [
        "com.oracle.truffle.r.native",
        "com.oracle.truffle.r.engine",
        "com.oracle.truffle.r.ffi.impl"
      ],
      "max_jobs" : "8",
      "native" : True,
      "workingSets" : "FastR",
      "buildDependencies" : ["FASTR"],
    },

    "com.oracle.truffle.r.test.tck" : {
      "sourceDirs" : ["src"],
      "dependencies" : [
        "mx:JUNIT",
        "sdk:POLYGLOT_TCK",
      ],
      "checkstyle" : "com.oracle.truffle.r.runtime",
      "javaCompliance" : "1.8+",
      "workingSets" : "FastR,Test",
    },
  },

  "distributions" : {
    "TRUFFLE_R_PARSER_PROCESSOR" : {
      "description" : "internal support for generating the R parser",
      "dependencies" : ["com.oracle.truffle.r.parser.processor"],
      "exclude" : [
        "ANTLR-3.5",
        "ANTLR-C-3.5",
       ],
       "maven" : "False",
    },

    "R_FFI_PROCESSOR" : {
      "description" : "internal support for generating FFI classes",
      "dependencies" : ["com.oracle.truffle.r.ffi.processor"],
      "maven" : "False",
    },

    "FASTR_LEGACY_LAUNCHER" : {
      "description" : "legacy launcher for the GraalVM",
      "dependencies" : ["com.oracle.truffle.r.legacylauncher"],
      "distDependencies" : [
        "sdk:GRAAL_SDK"
      ],
    },

    "FASTR_LAUNCHER" : {
      "description" : "launcher for the GraalVM (at the moment used only when native image is installed)",
      "dependencies" : ["com.oracle.truffle.r.launcher"],
      "distDependencies" : [
        "sdk:GRAAL_SDK"
      ],
    },

    "FASTR" : {
      "description" : "class files for compiling against FastR in a separate suite",
      "dependencies" : [
        "com.oracle.truffle.r.engine",
        "com.oracle.truffle.r.launcher",
        "com.oracle.truffle.r.ffi.impl"
      ],
      "mainClass" : "com.oracle.truffle.r.launcher.RCommand",
      "exclude" : [
        "truffle:JLINE",
        "ANTLR-3.5",
        "GNUR",
        "XZ-1.6",
      ],
      "distDependencies" : [
        "truffle:TRUFFLE_API",
        "truffle:TRUFFLE_NFI",
        "truffle:TRUFFLE_NFI_NATIVE",
      ],
    },

    "GRID_DEVICE_REMOTE_SERVER" : {
      "description" : "remote server for grid device",
      "dependencies" : [
        "com.oracle.truffle.r.library.fastrGrid.server",
      ],
      "mainClass" : "com.oracle.truffle.r.library.fastrGrid.server.RemoteDeviceServer",
      "exclude" : [
        "truffle:JLINE",
        "ANTLR-3.5",
        "GNUR",
        "XZ-1.6",
      ],
    },

    "FASTR_UNIT_TESTS" : {
      "description" : "unit tests",
      "dependencies" : [
        "com.oracle.truffle.r.test",
        "com.oracle.truffle.r.nodes.test"
       ],
      "exclude": ["mx:HAMCREST", "mx:JUNIT"],
      "distDependencies" : [
        "FASTR",
        "truffle:TRUFFLE_API",
        "TRUFFLE_R_PARSER_PROCESSOR",
        "truffle:TRUFFLE_TCK",
      ],


    },

    "FASTR_UNIT_TESTS_NATIVE" : {
      "description" : "unit tests support (from test.native project)",
       "native" : True,
       "platformDependent" : True,
      "dependencies" : [
        "com.oracle.truffle.r.test.native",
     ],
    },

    "TRUFFLE_R_TCK" : {
      "description" : "TCK tests provider",
      "dependencies" : [
        "com.oracle.truffle.r.test.tck"
      ],
      "exclude" : [
        "mx:JUNIT",
      ],
      "distDependencies" : [
        "sdk:POLYGLOT_TCK",
      ],
      "maven" : False
    },

    # see mx_fastr_dists.mx_register_dynamic_suite_constituents for the definitions of some RFFI-dependent distributions
  },
}
