/*
 * Copyright (c) 2013, 2018, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 3 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 3 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.builtin.base.infix;

import static com.oracle.truffle.r.runtime.RDispatch.INTERNAL_GENERIC;
import static com.oracle.truffle.r.runtime.builtins.RBehavior.PURE;
import static com.oracle.truffle.r.runtime.builtins.RBuiltinKind.PRIMITIVE;

import com.oracle.truffle.api.CompilerDirectives;
import com.oracle.truffle.api.Truffle;
import com.oracle.truffle.api.dsl.Fallback;
import com.oracle.truffle.api.dsl.NodeChild;
import com.oracle.truffle.api.dsl.Specialization;
import com.oracle.truffle.api.frame.FrameDescriptor;
import com.oracle.truffle.api.frame.FrameInstance.FrameAccess;
import com.oracle.truffle.api.frame.VirtualFrame;
import com.oracle.truffle.api.nodes.NodeCost;
import com.oracle.truffle.api.nodes.NodeInfo;
import com.oracle.truffle.api.profiles.BranchProfile;
import com.oracle.truffle.api.profiles.ConditionProfile;
import com.oracle.truffle.r.nodes.access.vector.ElementAccessMode;
import com.oracle.truffle.r.nodes.access.vector.ExtractVectorNode;
import com.oracle.truffle.r.nodes.builtin.RBuiltinNode;
import com.oracle.truffle.r.nodes.helpers.AccessListField;
import com.oracle.truffle.r.runtime.ArgumentsSignature;
import com.oracle.truffle.r.runtime.RError;
import com.oracle.truffle.r.runtime.builtins.RBuiltin;
import com.oracle.truffle.r.runtime.builtins.RSpecialFactory;
import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.data.RList;
import com.oracle.truffle.r.runtime.data.model.RAbstractListVector;
import com.oracle.truffle.r.runtime.data.model.RAbstractVector;
import com.oracle.truffle.r.runtime.env.REnvironment;
import com.oracle.truffle.r.runtime.nodes.RNode;

import qir.ast.QIRLambda;
import qir.ast.QIRVariable;
import qir.ast.data.QIRRcons;
import qir.ast.data.QIRRdestr;
import qir.ast.data.QIRRnil;
import qir.ast.operator.QIRProject;

@NodeInfo(cost = NodeCost.NONE)
@NodeChild(value = "arguments", type = RNode[].class)
abstract class AccessFieldSpecial extends RNode {

    @Child private AccessListField accessListField;

    @Specialization
    public Object doList(RList list, String field) {
        // Note: special call construction turns lookups into string constants for field accesses
        if (accessListField == null) {
            CompilerDirectives.transferToInterpreterAndInvalidate();
            accessListField = insert(AccessListField.create());
        }
        Object result = accessListField.execute(list, field);
        if (result == null) {
            throw RSpecialFactory.throwFullCallNeeded();
        } else {
            return result;
        }
    }

    @Fallback
    @SuppressWarnings("unused")
    public Object doFallback(Object container, Object field) {
        throw RSpecialFactory.throwFullCallNeeded();
    }
}

@RBuiltin(name = "$", kind = PRIMITIVE, parameterNames = {"", ""}, isFieldAccess = true, dispatch = INTERNAL_GENERIC, behavior = PURE)
public abstract class AccessField extends RBuiltinNode.Arg2 {

    @Child private ExtractVectorNode extract = ExtractVectorNode.create(ElementAccessMode.FIELD_SUBSCRIPT, true);

    private final ConditionProfile invalidAtomicVector = ConditionProfile.createBinaryProfile();
    private final BranchProfile error = BranchProfile.create();

    static {
        Casts.noCasts(AccessField.class);
    }

    public static RNode createSpecial(ArgumentsSignature signature, RNode[] arguments, @SuppressWarnings("unused") boolean inReplacement) {
        return signature.getNonNullCount() == 0 ? AccessFieldSpecialNodeGen.create(arguments) : null;
    }

    @Specialization
    protected Object access(Object container, String field) {
        if (!invalidAtomicVector.profile(container instanceof RAbstractListVector) && container instanceof RAbstractVector) {
            error.enter();
            throw error(RError.Message.DOLLAR_ATOMIC_VECTORS);
        } else if (container instanceof REnvironment && ((REnvironment) container).get("queryId") != null) {
            final Object queryId = ((REnvironment) container).get("queryId");
            if (queryId != null) {
                final QIRVariable v = new QIRVariable(null, "t");
                RContext.queries.set((int) queryId, new QIRProject(getSourceSection(),
                                new QIRLambda(null, null, v, new QIRRcons(null, field, new QIRRdestr(null, v, field), QIRRnil.getInstance()), new FrameDescriptor()),
                                RContext.queries.get((int) queryId)));
                RContext.envs.set((int) queryId, (VirtualFrame) Truffle.getRuntime().getCurrentFrame().getFrame(FrameAccess.MATERIALIZE).getArguments()[0]);
                return container;
            }
        }
        return extract.applyAccessField(container, field);
    }
}
