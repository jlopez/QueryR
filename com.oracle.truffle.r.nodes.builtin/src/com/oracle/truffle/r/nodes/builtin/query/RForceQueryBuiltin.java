/*
 * Copyright (c) 2012, 2015, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package com.oracle.truffle.r.nodes.builtin.query;

import qir.ast.QIRNode;

import static com.oracle.truffle.r.runtime.RVisibility.OFF;
import static com.oracle.truffle.r.runtime.builtins.RBehavior.COMPLEX;
import static com.oracle.truffle.r.runtime.builtins.RBuiltinKind.PRIMITIVE;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.oracle.truffle.api.CompilerDirectives.TruffleBoundary;
import com.oracle.truffle.api.dsl.*;
import com.oracle.truffle.api.source.Source;
import com.oracle.truffle.r.nodes.builtin.RBuiltinNode;
import com.oracle.truffle.r.nodes.qirinterface.QIRInterface;
import com.oracle.truffle.r.runtime.RRuntime;
import com.oracle.truffle.r.runtime.builtins.RBuiltin;
import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.data.RList;
import com.oracle.truffle.r.runtime.env.REnvironment;

/**
 * This builtin takes a query and returns a data frame that are the results of this query.
 */
@RBuiltin(name = "query.force", visibility = OFF, kind = PRIMITIVE, parameterNames = {"query"}, behavior = COMPLEX)
public abstract class RForceQueryBuiltin extends RBuiltinNode.Arg1 {
    static {
        Casts.noCasts(RForceQueryBuiltin.class);
    }

    @TruffleBoundary
    @Specialization
    public static final RList force(final REnvironment rQuery) {
        // Every query is identified by an integer included in the object returned by the query.
        final Object tmp = rQuery.get("queryId");
        final int queryId = tmp != null ? (int) tmp : RContext.INVALID_QUERY_ID;
        if (queryId == RContext.INVALID_QUERY_ID)
            throw new RuntimeException("Not a query object.");

        // TODO: Handle results given in streaming.
        QIRNode query = RContext.queries.get(queryId);
        if (query == null)
            throw new RuntimeException("Unrecognized or empty query: " + queryId);

        final RList cachedResults = RContext.results.get(queryId);
        if (cachedResults != null) // The query has been run before.
            return cachedResults;
        query = QIRInterface.normalize(query, RContext.envs.get(queryId));
        RContext.queries.set(queryId, query);

        final Map<String, List<Object>> rawResults = QIRInterface.run(query);
        // Create the data frame. TODO: Find a simpler way.
        final String frameCode = rawResults.entrySet().stream().map(
                        entry -> entry.getKey() + "=c(" +
                                        String.join(",", entry.getValue().stream().map(x -> x instanceof String ? "\"" + x + "\"" : x.toString()).collect(Collectors.toList())) +
                                        ")").collect(Collectors.joining("\n")) +
                        "\ndata.frame(" + String.join(", ", rawResults.keySet()) + ")";
        final RList queryResults = (RList) RContext.getEngine().parseAndEval(
                        Source.newBuilder(frameCode).name("ShouldNotExistBuilder").mimeType(RRuntime.R_APP_MIME).build(),
                        REnvironment.baseEnv().getFrame(), false);
        RContext.results.set(queryId, queryResults);

        return queryResults;
    }
}