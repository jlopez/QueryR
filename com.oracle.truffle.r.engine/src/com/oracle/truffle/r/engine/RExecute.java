package com.oracle.truffle.r.engine;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.hive.ql.exec.UDF;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;

import com.oracle.truffle.r.runtime.context.RContext;
import com.oracle.truffle.r.runtime.data.model.RAbstractContainer;

public class RExecute extends UDF {
    public static final Map<String, String> valueCache = new HashMap<>();

    public static String evaluate(final String program) {
        if (TruffleRLanguageImpl.rContext == null)
            try {
                TruffleRLanguageImpl.rContext = Context.newBuilder("R").build();
            } catch (IllegalArgumentException e) {
                throw new RuntimeException(e.getMessage());
            }
        if (valueCache.containsKey(program))
            return valueCache.get(program);
        RContext.setDeepEmbedded();
        try {
            final org.graalvm.polyglot.Source source = org.graalvm.polyglot.Source.newBuilder("R", program.toString(), "mySrc").build();
            final Value result = TruffleRLanguageImpl.rContext.eval(source);
            Object res = result.getReceiver();
            if (res instanceof RAbstractContainer) {
                final RAbstractContainer rac = (RAbstractContainer) res;
                if (rac.getLength() != 1)
                    throw new RuntimeException("Unhandled value: " + rac.toString());
                res = rac.getDataAtAsObject(0);
            }
            final String finalResult = res instanceof Serializable ? res.toString() : result.getSourceLocation().getCharacters().toString();
            valueCache.put(program, finalResult);
            return finalResult;
        } catch (Exception e) {
            e.printStackTrace();
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            throw new RuntimeException(sw.toString());
        }
    }
}
