package com.oracle.truffle.r.engine;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.hadoop.hive.ql.exec.UDF;

public class RExecuteApply extends UDF {
    public static String evaluate(final String fun, final List<String> args) {
        return RExecute.evaluate("f = " + fun + ";f(" + args.stream().map(arg -> arg.toString()).collect(Collectors.joining(", ")) + ")");
    }
}
