package com.oracle.truffle.r.runtime.nodes;

public interface IRSyntaxNodeVisitor<T> {
    public abstract T visit(final RSyntaxNode node);
}
