package com.oracle.truffle.r.runtime;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Base64;

public class RValue implements SQLData {
    private Serializable value = null;
    private String base64 = null;
    private String typeName = null;

    public RValue() {
    }

    public RValue(Serializable value) throws IOException {
        this.value = value;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(value);
        oos.close();
        base64 = Base64.getEncoder().encodeToString(baos.toByteArray());
    }

    public final Serializable getValue() {
        return this.value;
    }

    @Override
    public final String getSQLTypeName() throws SQLException {
        return this.typeName;
    }

    @Override
    public final void readSQL(SQLInput stream, String type) throws SQLException {
        base64 = stream.readString();
        byte[] data = Base64.getDecoder().decode(base64);
        try {
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
            value = (Serializable) ois.readObject();
            ois.close();
            typeName = type;
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public final void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(base64);
    }
}
