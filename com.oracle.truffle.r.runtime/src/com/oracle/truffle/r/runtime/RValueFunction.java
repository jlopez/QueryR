package com.oracle.truffle.r.runtime;

import java.io.Serializable;

import com.oracle.truffle.api.frame.MaterializedFrame;

public class RValueFunction implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String name;
    private final String code;
    private final MaterializedFrame frame;

    public RValueFunction(final String name, final String code, final MaterializedFrame frame) {
        this.name = name;
        this.code = code;
        this.frame = frame;
    }

    public final String getName() {
        return name;
    }

    public final String getCode() {
        return code;
    }

    public final MaterializedFrame getDependencies() {
        return frame;
    }
}
